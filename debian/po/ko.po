# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Korean messages for debian-installer.
# Copyright (C) 2003,2004,2005,2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Changwoo Ryu <cwryu@debian.org>, 2010-2012, 2014-2015, 2017-2018, 2020-2025.
#
# Translations from iso-codes:
# Copyright (C)
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Changwoo Ryu <cwryu@debian.org>, 2004, 2008, 2009, 2010, 2011.
#   Copyright (C) 2000 Free Software Foundation, Inc.
#   Eungkyu Song <eungkyu@sparcs.org>, 2001.
#   Free Software Foundation, Inc., 2001,2003
#   Jaegeum Choe <baedaron@hananet.net>, 2001.
#   (translations from drakfw)
#   Kang, JeongHee <Keizi@mail.co.kr>, 2000.
#   Sunjae Park <darehanl@gmail.com>, 2006-2007.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: systemd-boot-installer@packages.debian.org\n"
"POT-Creation-Date: 2025-01-04 18:06+0100\n"
"PO-Revision-Date: 2025-01-01 23:35+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team:  Korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl4:
#: ../systemd-boot-installer.templates:1001
msgid "Install the systemd-boot boot loader"
msgstr "systemd-boot 부트로더 설치"

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid "Number of retries per boot entry (0 to disable):"
msgstr "부팅 항목마다 재시도할 횟수 (0이면 사용하지 않음):"

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid ""
"Configures the automatic boot assessment logic, which automatically falls "
"back to a previous boot entry if a new one fails more than the configured "
"number of retries when first added. For more details, see: https://systemd."
"io/AUTOMATIC_BOOT_ASSESSMENT/"
msgstr ""
"자동 부팅 평가 로직을 설정합니다. 이 로직은 만약 새로운 부팅 항목이 추가된 이"
"후 설정한 재시도 횟수보다 더 많이 실패하면 이전의 부팅 항목으로 돌아갑니다. "
"참고: https://systemd.io/AUTOMATIC_BOOT_ASSESSMENT/"
