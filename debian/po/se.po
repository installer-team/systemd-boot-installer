# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of se.po to Northern Saami
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Børre Gaup <boerre@skolelinux.no>, 2006, 2010.
msgid ""
msgstr ""
"Project-Id-Version: se\n"
"Report-Msgid-Bugs-To: systemd-boot-installer@packages.debian.org\n"
"POT-Creation-Date: 2025-01-04 18:06+0100\n"
"PO-Revision-Date: 2021-05-21 05:32+0000\n"
"Last-Translator: Jacque Fresco <aidter@use.startmail.com>\n"
"Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n == 2) ? 1 : 2);\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl4:
#: ../systemd-boot-installer.templates:1001
#, fuzzy
msgid "Install the systemd-boot boot loader"
msgstr "Sajáiduhttimin vuolggáhangieđahalli GRUB"

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid "Number of retries per boot entry (0 to disable):"
msgstr ""

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid ""
"Configures the automatic boot assessment logic, which automatically falls "
"back to a previous boot entry if a new one fails more than the configured "
"number of retries when first added. For more details, see: https://systemd."
"io/AUTOMATIC_BOOT_ASSESSMENT/"
msgstr ""
