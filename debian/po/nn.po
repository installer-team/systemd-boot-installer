# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Norwegian Nynorsk translation of debian-installer.
# Copyright (C) 2003–2010 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2004, 2005, 2006, 2007, 2008.
# Eirik U. Birkeland <eirbir@gmail.com>, 2010.
# Allan Nordhøy <epost@anotheragency.no>, 2017.
# Alexander Jansen <bornxlo@gmail.com>, 2018.
# aujawindar <eivind.odegard@sogn.no>, 2022.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Free Software Foundation, Inc., 2001, 2004.
#   Håvard Korsvoll <korsvoll@gmail.com>, 2004,2006, 2007.
#   Karl Ove Hufthammer <karl@huftis.org>, 2003-2004, 2006. (New translation done from scratch.).
#   Kjartan Maraas  <kmaraas@gnome.org>, 2001.
#   Roy-Magne Mo <rmo@sunnmore.net>, 2001.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
msgid ""
msgstr ""
"Project-Id-Version: nn\n"
"Report-Msgid-Bugs-To: systemd-boot-installer@packages.debian.org\n"
"POT-Creation-Date: 2025-01-04 18:06+0100\n"
"PO-Revision-Date: 2024-03-19 11:01+0000\n"
"Last-Translator: Ola Haugen Havrevoll <havrevoll@gmail.com>\n"
"Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl4:
#: ../systemd-boot-installer.templates:1001
#, fuzzy
msgid "Install the systemd-boot boot loader"
msgstr "Installerer oppstartssystemet Aboot"

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid "Number of retries per boot entry (0 to disable):"
msgstr ""

#. Type: select
#. Description
#. :sl4:
#: ../systemd-boot-installer.templates:3001
msgid ""
"Configures the automatic boot assessment logic, which automatically falls "
"back to a previous boot entry if a new one fails more than the configured "
"number of retries when first added. For more details, see: https://systemd."
"io/AUTOMATIC_BOOT_ASSESSMENT/"
msgstr ""
